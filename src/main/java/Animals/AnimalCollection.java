package zprogramowanie_poziom_1.Animals;

import sda_wo3.Animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnimalCollection {
    static List<AbstracAnimal> animalList = new ArrayList<>();

    public void addAnimal(AbstracAnimal abstracAnimal) {
        animalList.add(abstracAnimal);
    }

    public void removeAnima(int index) {
        animalList.remove(index);
    }

    public void moveAll() {
        for (AbstracAnimal animal : animalList) {
            animal.move();
        }

    }

}



