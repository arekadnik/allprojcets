package zprogramowanie_poziom_1.Animals;

import sda_wo4.generics.AbstractAnimal;

public class Dog extends AbstracAnimal {


    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }

    public Dog(String name) {
        super(name);
    }
    @Override
    public void move() {
        System.out.println(name);
        System.out.println("hau hau");
    }
}
