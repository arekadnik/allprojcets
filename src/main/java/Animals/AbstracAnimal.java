package zprogramowanie_poziom_1.Animals;

public abstract class AbstracAnimal {
    protected String name;

    public AbstracAnimal(String name) {
        this.name = name;
    }

    public abstract void move();
}
