package zprogramowanie_poziom_1.Animals;

public class Cat extends AbstracAnimal {
    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                '}';
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println(name);
        System.out.println("Miau miau");
    }
}
