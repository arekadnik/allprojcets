package jdbcExcercise.entities;

public class Car implements Entity{

    private String model;
    private String type;
    private int engineCapacity;
    private int howManyPeople;

    public Car(String model, String type, int engineCapacity, int howManyPeople) {
        this.model = model;
        this.type = type;
        this.engineCapacity = engineCapacity;
        this.howManyPeople = howManyPeople;
    }
    public Car(){

    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getHowManyPeople() {
        return howManyPeople;
    }

    public void setHowManyPeople(int howManyPeople) {
        this.howManyPeople = howManyPeople;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", type='" + type + '\'' +
                ", engineCapacity=" + engineCapacity +
                ", howManyPeople=" + howManyPeople +
                '}';
    }
}
