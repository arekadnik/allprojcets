package jdbcExcercise.entities;

public class Product implements Entity {
    private String name;
    private int price;
    private String supplier;

    public Product(String name, int price, String supplier) {
        this.name = name;
        this.price = price;
        this.supplier = supplier;
    }
    public Product(){

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", supplier='" + supplier + '\'' +
                '}';
    }
}
