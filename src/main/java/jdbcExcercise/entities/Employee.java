package jdbcExcercise.entities;

public class Employee implements Entity{
    private int id;
    private String firstName;
    private String lastName;
    private int age;


    public Employee(int id, String fristname, String lastname, int age) {
        this.age = age;
        this.firstName = fristname;
        this.lastName = lastname;
        this.id = id;
    }

    public Employee() {

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "age=" + age +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", id=" + id +
                '}';
    }
}
