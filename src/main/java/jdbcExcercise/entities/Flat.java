package jdbcExcercise.entities;


public class Flat implements Entity{

    private int price;
    private int surface;
    private int numberOfRooms;
    private String adresses;


    public Flat(int price, int surface, int numberOfRooms, String adresses) {
        this.price = price;
        this.surface = surface;
        this.numberOfRooms = numberOfRooms;
        this.adresses = adresses;
    }

    public Flat() {

    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getAdresses() {
        return adresses;
    }

    public void setAdresses(String adresses) {
        this.adresses = adresses;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "price=" + price +
                ", surface=" + surface +
                ", numberOfRooms=" + numberOfRooms +
                ", adresses='" + adresses + '\'' +
                '}';
    }
}
