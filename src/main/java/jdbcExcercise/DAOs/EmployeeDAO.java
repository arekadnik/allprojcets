package jdbcExcercise.DAOs;

import jdbcExcercise.BaseDAO;
import jdbcExcercise.entities.Employee;
import jdbcExcercise.sqlGenerators.EmployeeSqlGenerator;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO extends BaseDAO<Employee> {

    private EmployeeSqlGenerator employeeSqlGenerator = new EmployeeSqlGenerator();

    @Override
    public void insert(Employee toInsert) {

        sql = employeeSqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Employee> select() {
        sql= employeeSqlGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Employee toUpdate) {
        sql = employeeSqlGenerator.update(toUpdate);
        execute();

    }

    @Override
    public void delete(Employee toDelete) {

        sql = employeeSqlGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Employee> parse(ResultSet resultSet) {
        ArrayList<Employee> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                int age = resultSet.getInt("age");
                result.add(new Employee(id, firstname, lastname, age));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}

