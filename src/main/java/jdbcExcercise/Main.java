package jdbcExcercise;

import java.sql.*;

public class Main {

    private static final String JDBC_DRIVER = "com.mysql.jdbcExercise.DAOimplementation.Driver";
    private static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/EMP";
    private static final String USER = "root";
    private static final String PASS = "admin";
    public static void main(String[] args) {

        Connection conn = null;
        Statement stmt = null;

        //rejestrowanie sterownika
        try {
            //dla starych projektow potrzebne
//            Class.forName(JDBC_DRIVER);
            //2.Nawiaywanie polaczenia
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //3.zapytanie
            stmt = conn.createStatement();
            String sql = "SELECT * FROM Employees";

            //3a.Wykonanie zapytania
            ResultSet rs = stmt.executeQuery(sql);
            //4.Rozpakowywnaie danych
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                int age = rs.getInt("age");

                System.out.printf("ID: %d, Imie: %s, Nazwisko: %s, Wiek: %d \n",
                        id, firstname, lastname, age);
            }
            //spratamy i zamykamy polaczenie
            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
            //double check zeby wszystko sie zamknelo
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
