package jdbcExcercise;

import jdbcExcercise.entities.Entity;


import java.sql.*;
import java.util.List;

public abstract class BaseDAO <T extends Entity> {

    private static final String JDBC_DRIVER = "com.mysql.jdbcExercise.DAOimplementation.Driver";
    private static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/EMP";
    private static final String USER = "root";
    private static final String PASS = "admin";
    protected String sql;

    public abstract void insert(T toInsert);

    public abstract List<T> select();

    public abstract void update(T toupdatede);

    public abstract void delete(T toupdatede);

    protected abstract List<T> parse(ResultSet resultSet);


    protected void execute() {
        try {
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected List<T> executeSelect(){

        try {
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            List<T> result = parse(resultSet);
            resultSet.close();
            statement.close();
            connection.close();
            return result;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

}
