package jdbcExcercise.UnitTests;
import jdbcExercise.DAOimplementation.DAOs.EmployeeDAO;
import jdbcExercise.DAOimplementation.entities.Employee;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class EmployeeDAOTests {


    @Test
    public void insertEmployeeTest() throws Exception {
        Employee employee = new Employee(999,"AAA","DDDD",1);
        EmployeeDAO employeeDAO = new EmployeeDAO();
        List<Employee> employeeList = employeeDAO.select();
        for (Employee emp:employeeList) {
            if(emp.getId() == employee.getId()){
                throw new Exception("This is bad");
            }
        }
        boolean isOk = false;
        employeeDAO.insert(employee);
        List<Employee> employeeListafterInset = employeeDAO.select();
        for (Employee emp:employeeListafterInset) {
            if(emp.getId() == employee.getId()){
                isOk= true;
            }
        }
        Assert.assertTrue(isOk);
    }

    @Test
    public void deleteEmployTest() throws Exception {
        boolean isOk = false;
        Employee employee = new Employee(909,"AAA","DDDD",1);
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.insert(employee);
        List<Employee> employeeList = employeeDAO.select();
        for (Employee emp:employeeList) {
            if(emp.getId() == employee.getId()){
                isOk = true;
            }
        }
        Assert.assertTrue(isOk);

        employeeDAO.delete(employee);
        List<Employee> employeeListafterInset = employeeDAO.select();
        for (Employee emp:employeeListafterInset) {
            if (emp.getId() == employee.getId()) {
                throw  new Exception("This is very bad");
            }
        }

    }

}
