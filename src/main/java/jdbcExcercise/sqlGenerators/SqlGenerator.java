package jdbcExcercise.sqlGenerators;

import jdbcExcercise.entities.Entity;


public interface SqlGenerator<T extends Entity> {

    String insert(T toInsert);
    String selectAll();
    String update(T toUpdate);
    String delete(T toDelete);

}
