package jdbcExcercise.sqlGenerators;

import jdbcExcercise.entities.Employee;


public class CarSqlGenerator implements SqlGenerator<Employee> {

    public String insert(Employee toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into Employees ")
                .append("(id, firstname, lastname, age) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getFirstName()).append("'")
                .append(", '").append(toInsert.getLastName()).append("'")
                .append(", ").append(toInsert.getAge()).append(");");
        return sb.toString();
    }


    public String selectAll() {
        return "Select * from Employees;";
    }


    public String update(Employee toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE Employees SET firstname='")
                .append(toUpdate.getFirstName()).append("',")
                .append("lastname='").append(toUpdate.getLastName()).append("',")
                .append("age=").append(toUpdate.getAge())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }


    public String delete(Employee toDelete) {
        return "delete from Employees where id=" + toDelete.getId() + ";";
    }
}
