package sda_wo1;

public class Zadanie24 {
    public static void main(String[] args){


    int fib1 = fib(1);
    int fib2 = fib(2);
    int fib3 = fib(3);
    int fib4 = fib(5);
    int fib5 = fib(10);

        System.out.println("fib1 = " + fib1);
        System.out.println("fib2 = " + fib2);
        System.out.println("fib3 = " + fib3);
        System.out.println("fib4 = " + fib4);
        System.out.println("fib5 = " + fib5);
}

    public static int fib(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fib(n - 1) + fib(n - 2);
    }
}