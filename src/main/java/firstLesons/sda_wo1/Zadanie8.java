package sda_wo1;

public class Zadanie8 {
    public static void main(String[] args) {

        int sentence = first("dupajasiu", 'k');
        System.out.println(sentence);

    }

    public static int first(String tekst, char b) {

        int dlugoscZnaku = tekst.length();

        for (int i = 0; i < dlugoscZnaku; i++) {

            char search = tekst.charAt(i);

            if (search == b) {

                return i;
            }

        }
        return -1;
    }


}
