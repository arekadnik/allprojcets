package sda_wo1;

public class Zadanie23 {
    public static void main(String[] args){


        char[] toEncode = {'a', 'b', 'c', 'd'};

        char[] encoded = encode(toEncode, 24);

        printArray(encoded);
    }

    public static char[] encode(char[] toEncode, int shift) {
        char firstLetter = 'a';
        char lastLetter = 'z';

        char[] result = new char[toEncode.length];

        for (int i = 0; i < toEncode.length; i++) {
            int newLetterValue = toEncode[i] + shift;
            if (newLetterValue > lastLetter) {
                newLetterValue = newLetterValue + firstLetter - lastLetter - 1;
            }

            result[i] = (char) newLetterValue;
        }

        return result;
    }

    public static void printArray(char[] array) {
        for (char v : array) {
            System.out.print(v + " ");
        }
        System.out.println();
    }
}
