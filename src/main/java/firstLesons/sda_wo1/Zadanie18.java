package sda_wo1;

public class Zadanie18 {
    public static void main(String[] args){

        int [] arr1 = {1,2,3};
        int [] arr2 = {2,3,4};

        int [] myArray = newTest(arr1,arr2);

        printArray(myArray);

}

public static int [] newTest(int [] arg1, int [] arg2){

        int [] newArray = new int [arg1.length];

        for (int i = 0 ; i<arg1.length; i++){

            newArray[i]= arg1[i] + arg2[i];

        }
            return  newArray;

}

    public static void printArray (int [] array){

        for(int v :array){

            System.out.print(" " + v);

        }
        System.out.println();
    }


}
