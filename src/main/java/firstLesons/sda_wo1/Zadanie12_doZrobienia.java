package sda_wo1;

import com.sun.org.apache.xerces.internal.xs.StringList;

public class Zadanie12_doZrobienia {
    public static void main(String[] args) {

        String[] argg1 = {"a", "b", "c", "d"};
        String[] argg2 = {"e", "f", "g", "h"};

        String[] con2 = contains(argg1, argg2);
        printArray(con2);


    }

    public static String[] contains(String[] argg1, String[] argg2) {

        String[] newArray = new String[argg1.length + argg2.length];

        for (int i = 0; i < argg1.length; i++) {

            newArray[i] = argg1[i];
        }

        int index = 0;
        for (int i = argg1.length; i < newArray.length; i++) {

            newArray[i] = argg2[index];
        index++;
        }

        return newArray;

    }

    public static void printArray(String[] array) {

        for (String v : array) {
            System.out.print(v + " ");

        }
        System.out.println();


    }


}
