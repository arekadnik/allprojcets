package sda_wo1;

public class Klasy {
    public static void main(String[] args) {


        outerLoop:
        for (int j = 0; ; j += 100) {
            for (int i = 0; i < 5; i++) {
                if ((i + j) % 2 == 1)
                    continue ;

                if (j > 100)
                    break outerLoop; // przerywa wykonanie także pętli zewnętrznej

                System.out.println(i + j + " jest liczbą parzystą");
            }
        }
    }
}