package sda_wo1;

public class Zadanie20 {
    public static void main(String[] args) {



                int[][] array = {{1, 3, 2}, {3, 9}, {2, 9, 12}, {20}};

                int biggestRowIndex = findBiggestRowIndex(array);
                System.out.println("biggestRowIndex = " + biggestRowIndex);
            }

            public static int findBiggestRowIndex(int[][] array) {
                int min = Integer.MAX_VALUE;
                int index = -1;

                for (int i = 0; i < array.length; i++) {
                    int rowSum = 0;
                    for (int j = 0; j < array[i].length; j++) {
                        rowSum += array[i][j];
                    }
                    if (rowSum > min) {
                        min = rowSum;
                        index = i;
                    }
                }

                return index;
            }
        }


