package sda_wo1;

import java.util.Scanner;

public class Metody {
    public static void main(String[] args) {

        Scanner Info = new Scanner(System.in);
        System.out.println("First number");
        int FirstNumber = Info.nextInt();
        System.out.println("Choose char operation");
        Info.nextLine();
        char operation = Info.nextLine().charAt(0);
        System.out.println("Second number");
        int SecondNumber = Info.nextInt();

        int wynik = calculate(FirstNumber, operation, SecondNumber);
        System.out.println(wynik);

    }

    public static int calculate(int a, char operation, int b) {

        int result;

        if (operation == '+') {
            result = sum(a, b);
        } else if (operation == '-') {
            result = sub(a, b);
        } else if (operation == '*') {
            result = mul(a, b);
        } else if (operation == '/') {
            result = div(a, b);
        } else {
            result = 0;
        }

        return result;

    }


    public static int sum(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {

        return a - b;
    }

    public static int mul(int a, int b) {

        return a * b;
    }

    public static int div(int a, int b) {

        return a / b;
    }

}
