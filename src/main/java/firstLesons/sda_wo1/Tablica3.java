package sda_wo1;

import java.io.PrintStream;
import java.util.Scanner;

public class Tablica3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj liczbe wierszy");
        int rows = scanner.nextInt();
        System.out.println("podaj liczbe kolumn");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                array [i] [j] = i + j +2 ;

                System.out.print(array[i][j] + " ");

            }
            System.out.println();
        }
    }
}

