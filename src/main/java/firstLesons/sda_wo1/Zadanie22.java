package sda_wo1;

public class Zadanie22 {
    public static void main(String[] args){

        boolean[] eratosthenesSieve = eratosthenesSieve(50);

        boolean isPrime1 = eratosthenesSieve[2];
        boolean isPrime2 = eratosthenesSieve[3];
        boolean isPrime3 = eratosthenesSieve[4];
        boolean isPrime4 = eratosthenesSieve[5];
        boolean isPrime5 = eratosthenesSieve[8];
        boolean isPrime6 = eratosthenesSieve[15];
        boolean isPrime7 = eratosthenesSieve[23];
        boolean isPrime8 = eratosthenesSieve[35];
        boolean isPrime9 = eratosthenesSieve[37];

        System.out.println("isPrime1 = " + isPrime1);
        System.out.println("isPrime2 = " + isPrime2);
        System.out.println("isPrime3 = " + isPrime3);
        System.out.println("isPrime4 = " + isPrime4);
        System.out.println("isPrime5 = " + isPrime5);
        System.out.println("isPrime6 = " + isPrime6);
        System.out.println("isPrime7 = " + isPrime7);
        System.out.println("isPrime8 = " + isPrime8);
        System.out.println("isPrime9 = " + isPrime9);
    }

    public static boolean[] eratosthenesSieve(int n) {
        boolean[] result = new boolean[n];
        for (int i = 0; i < result.length; i++) {
            result[i] = true;
        }

        result[0] = false;
        result[1] = false;

        for (int i = 2; i < n; i++) {
            if (result[i]) {
                for (int j = 2 * i; j < n; j = j + i) {
                    result[j] = false;
                }
            }
        }

        return result;
    }
}
