package sda_wo1;

public class Zadanie21 {
    public static void main(String[] args){


        boolean isPalindrome1 = isPalindrome("kajak");
        boolean isPalindrome2 = isPalindrome("radar");
        boolean isPalindrome3 = isPalindrome("abba");
        boolean isPalindrome4 = isPalindrome("abecadlo");
        boolean isPalindrome5 = isPalindrome("abeebe");

        System.out.println("isPalindrome1 = " + isPalindrome1);
        System.out.println("isPalindrome2 = " + isPalindrome2);
        System.out.println("isPalindrome3 = " + isPalindrome3);
        System.out.println("isPalindrome4 = " + isPalindrome4);
        System.out.println("isPalindrome5 = " + isPalindrome5);
    }

    public static boolean isPalindrome(String text) {
        int size = text.length();
        for (int i = 0; i < size / 2; i++) {
            if (text.charAt(i) != text.charAt(size - i - 1)) {
                return false;
            }
        }

        return true;
    }
}