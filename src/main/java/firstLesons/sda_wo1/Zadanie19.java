package sda_wo1;

public class Zadanie19 {
    public static void main(String[] args) {
        int[] array1 = {2, 4, 7, 1, 32, 29};
        int[] array2 = {11, 43, 1, 21, 9};

        int[] array = sumArrays(array1, array2);
        printArray(array);


    }

    public static int[] sumArrays(int[] array1, int[] array2) {
        int[] shorterArray = getShorterArray(array1, array2);
        int[] longerArray = getLongerArray(array1, array2);

        int[] result = new int[longerArray.length];
        for (int i = 0; i < shorterArray.length; i++) {
            result[i] = shorterArray[i] + longerArray[i];
        }
        for (int i = shorterArray.length; i < longerArray.length; i++) {
            result[i] = longerArray[i];
        }

        return result;

    }

    public static int[] getShorterArray(int[] array1, int[] array2) {
        if (array1.length < array2.length) {
            return array1;
        }
        return array2;

    }
    public static int[] getLongerArray(int[] array1, int[] array2) {
        if (array1.length > array2.length) {
            return array1;
        }
        return array2;
    }
    public static void printArray(int[] array) {
        for (int v : array) {
            System.out.print(v + " ");
        }
        System.out.println();
    }
}
