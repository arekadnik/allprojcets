package sda_wo1;

public class Zadanie25 {
    public static void main(String[] args){

        int[][] matrix1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matrix2 = {{7, 8}, {9, 10}, {11, 12}};

        int[][] result = mul(matrix1, matrix2);
        printArray(result);
    }

    public static int[][] mul(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[matrix1.length][matrix2[0].length];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                int cellValue = 0;
                for (int k = 0; k < matrix2.length; k++) {
                    cellValue = cellValue + matrix1[i][k] * matrix2[k][j];
                }
                result[i][j] = cellValue;
            }
        }

        return result;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
