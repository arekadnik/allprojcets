package sda_wo2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Zadanie9 {
    public static void main(String[] args) throws IOException {


        String fileName = "dupa.txt";
        String text = "text to be written";

        writeText(text,fileName);



    }


    public static void  writeText (String text, String fileName) throws IOException {

        FileWriter fileWriter = new FileWriter(fileName);
        PrintWriter printWriter = new PrintWriter(fileWriter);



        printWriter.println(text);
        printWriter.close();
        fileWriter.close();
    }
}
