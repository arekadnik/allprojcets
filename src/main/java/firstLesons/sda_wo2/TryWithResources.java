package sda_wo2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TryWithResources {
    public static void main(String[] args) throws IOException {
        String fileName = "file_name.txt";
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String currentLine = reader.readLine();
            while (currentLine != null) {
                System.out.println("currentLine = " + currentLine);
                currentLine = reader.readLine();
            }
        }


    }
}
