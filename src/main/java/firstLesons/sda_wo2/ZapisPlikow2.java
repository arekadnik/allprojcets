package sda_wo2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ZapisPlikow2 {
    public static void main(String[] args) throws IOException {


        String file = "abc.txt";
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("another text to be written");
        printWriter.close();
        fileWriter.close();
    }
}
