package sda_wo2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ZapisPlikow {

    public static void main(String[] args) throws IOException {

        String file = "abc.txt";
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        writer.write("text to be written");
        writer.close();
        fileWriter.close();
    }
}
