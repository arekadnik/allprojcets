package sda_wo2;

import java.io.*;
import java.util.Scanner;

public class Zadanie14 {
    public Zadanie14() throws FileNotFoundException, IOException {
    }

    public static void main(String[] args) throws IOException {

        String fileName = "dupa.txt";
        fileReader(fileName);

    }

    public static void fileReader( String fileName) throws IOException {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                String currentLine = scanner.nextLine();
                System.out.println(currentLine);
            }

        }
    }
}
