package sda_wo2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Odczytplikow2 {
    public static void main(String[] args) throws FileNotFoundException {


        String file = "abc.txt";
        Scanner scanner = new Scanner(new File(file));
        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            System.out.println("currentLine = " + currentLine);
        }
    }
}