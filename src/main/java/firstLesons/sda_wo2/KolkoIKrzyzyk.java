package sda_wo2;

import java.util.Scanner;

public class KolkoIKrzyzyk {
    public static void main(String[] args) {

        {
            Scanner scanner = new Scanner(System.in);
            playGame();
            String doYouWantPlayAgain = doYouWantPlayAgain(scanner);
            do {
                playGame();
            } while (doYouWantPlayAgain.equals("y"));

        }


    }


    public static void playGame() {

        Scanner scanner = new Scanner(System.in);
        String currentPlayer = choosePlayer(scanner);
        String[][] board = createBoard();
        printBoard(createBoard());
        boolean takenCells = areAllCellsTaken(board);
        boolean win = areYouWinner(board);
        while (!win && !takenCells) {
            int[] a = getPosition(scanner);
            setSymbol(currentPlayer, board, a);
            printBoard(board);
            currentPlayer = getNextPlayer(currentPlayer);
            takenCells = areAllCellsTaken(board);
            win = areYouWinner(board);
        }
        if (win == true){

            System.out.println("gratulacje wygrał zawodnik" + " " + currentPlayer );
        }
    }


    public static String doYouWantPlayAgain(Scanner scanner) {

        System.out.println("Do you want play again y/n ?");
        String n = scanner.nextLine();
        return n;
    }


    public static String getNextPlayer(String currentPlayer) {

        if (currentPlayer.equals("x")) {
            return "o";
        }
        return "x";
    }


    public static boolean areYouWinner(String[][] board) {

        if (board[0][0].equals(board[0][1]) && (board[0][0].equals(board[0][2]) && !board[0][0].equals(" "))) {
            return true;
        }
        if (board[1][0].equals(board[1][1]) && (board[1][0].equals(board[1][2]) && !board[1][0].equals(" "))) {
            return true;
        }
        if (board[2][0].equals(board[2][1]) && (board[2][0].equals(board[2][2]) && !board[2][0].equals(" "))) {
            return true;
        }
        if (board[0][0].equals(board[1][0]) && (board[0][0].equals(board[2][0]) && !board[0][0].equals(" "))) {
            return true;
        }
        if (board[0][1].equals(board[1][1]) && (board[0][1].equals(board[2][1]) && !board[0][1].equals(" "))) {
            return true;
        }
        if (board[0][2].equals(board[1][2]) && (board[0][2].equals(board[2][2]) && !board[0][2].equals(" "))) {
            return true;
        }

        if (board[2][0].equals(board[1][1]) && (board[2][0].equals(board[0][2]) && !board[2][0].equals(" "))) {
            return true;
        }
        if (board[0][0].equals(board[1][1]) && (board[0][0].equals(board[2][2]) && !board[0][0].equals(" "))) {
            return true;
        }
        return false;
    }

    public static boolean areAllCellsTaken(String[][] board) {


        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j].equals(" ")) {
                    return false;
                }

            }

        }
        return true;
    }

    public static void setSymbol(String currentPlayer, String[][] board, int[] a) {

        int row = a[0];
        int colummn = a[1];

        board[row][colummn] = currentPlayer;


    }

    public static int[] getPosition(Scanner scanner) {

        System.out.println("Wybierz pozyje");

        int[] board = new int[2];
        board[0] = scanner.nextInt();
        board[1] = scanner.nextInt();
        scanner.nextLine();
        return board;


    }


    public static String choosePlayer(Scanner scanner) {

        System.out.println("Ktory gracz zaczyna x/o ?");
        String n = scanner.nextLine();
        return n;


    }


    public static String[][] createBoard() {

        String[][] myBoard = new String[3][3];
        for (int i = 0; i < myBoard.length; i++) {
            for (int j = 0; j < myBoard.length; j++) {
                myBoard[i][j] = " ";
//                String[][] myBoard = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
            }
        }
        return myBoard;

    }


    public static void printBoard(String[][] board) {

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j]);
                if (j < board.length - 1) {

                    System.out.print("|");
                }
            }
            System.out.println();
            if (i < board.length - 1) {
                System.out.println("------");
            }
        }

    }


}
