package sda_wo2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class OdczytPlikow {
    public static void main(String[] args) throws IOException {


        String file = "abc.txt";
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String currentLine = reader.readLine();
        while (currentLine != null) {
            System.out.println("currentLine = " + currentLine);
            currentLine = reader.readLine();
        }
        reader.close();
        fileReader.close();


    }
}
