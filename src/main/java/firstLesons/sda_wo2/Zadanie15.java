package sda_wo2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie15 {
    public static void main(String[] args) {

        String fileName = "aaaaaa.txt";

        try {
            readFromFile(fileName);
        } catch (FileNotFoundException e) {
            System.out.println("e = " + e);


            try {

                neProblem();
            } catch (NumberFormatException e1) {
                System.out.println("e1 = " + e1);


                try {
                    newProblem();
                } catch (NullPointerException e2) {
                    System.out.println("e2 = " + e2);
                }
            }
        }

    }


    public static void readFromFile(String fileName) throws FileNotFoundException {


        Scanner scanner = new Scanner((new File(fileName)));
        while (scanner.hasNextLine()) {
            String currentLine = scanner.nextLine();
            System.out.println(currentLine);


        }
    }


    public static void neProblem() {

        String a = "arek";
        int d = Integer.parseInt(a);

    }

    public static void newProblem() throws NullPointerException {

        String a = null;
        a.charAt(0);

    }
}