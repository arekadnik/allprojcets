package sda_wo2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zadanie11 {
    public static void main(String[] args) throws IOException {


        Scanner scanner = new Scanner(System.in);
        int n = howManyLines(scanner);
        String output = getTextFromUser(n, scanner);
        doYouWantToSave(output, scanner);
    }


    public static void doYouWantToSave(String text, Scanner scanner) throws IOException {


        System.out.println("Chcesz zapisac plik?");
        String word = scanner.nextLine();


        if (word.equals("n")) {

            System.out.println("dobranoc");

        } else if (word.equals("t")) {

            System.out.println("proszę podaj nazwe pliku");
            String textName = scanner.nextLine();
            FileWriter fileWriter = new FileWriter(textName);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            printWriter.println(text);
            printWriter.close();
            fileWriter.close();
        }


    }


    public static String getTextFromUser(int n, Scanner scanner) {


        StringBuilder outPut = new StringBuilder();


        for (int i = 0; i < n; i++) {
            System.out.println("Wpisz prosze linie" + "  " + (i + 1));
            String word = scanner.nextLine();
            outPut.append(word).append("\n");
        }
        return outPut.toString();
    }

    public static int howManyLines(Scanner scanner) {

        System.out.println("Ile linii tekstu chesz wprowadzic");
        int n = scanner.nextInt();
        scanner.nextLine();
        return n;

    }


}

