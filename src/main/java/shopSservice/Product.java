package shopSservice;

public class Product {
    int serial;
    private String name;

    public Product(int serial, String name) {
        this.serial = serial;
        this.name = name;
    }

    public int getSerial() {
        return serial;
    }

    public String getName() {
        return name;
    }
}
