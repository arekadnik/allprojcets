package shopSservice;


import java.util.*;

public class ShopDao {

    private Map<Category, List<Product>> db = new HashMap<>();

    void addCategory(Category category) {
        db.put(category, new ArrayList<>());
    }

    void remove(Category category) {
        db.remove(category);
    }

    void addProduct(Category category, Product product) {
        if (!db.containsKey(category)) {
            addCategory(category);
        }
        db.get(category).add(product);
    }

    void removeProduct(Category category, Product product) {
        db.get(category).remove(product);

    }

    List<Product> getProduct(Category category) {

        return db.get(category);

    }

    Set<Category> allCategories() {
        for (Category c:db.keySet()) {
            System.out.println(c.toString());
        }
        return db.keySet();
    }


    public void removeCategory(Category category) {

        db.remove(category);
    }
}

