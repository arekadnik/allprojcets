package shopSservice;

import java.util.Scanner;

public class Gui {
    private ShopService service;
    private Scanner scanner = new Scanner(System.in);

    public Gui(ShopService service) {
        this.service = service;
    }

    void start() {
        int input;
        do {
            String choose;
            System.out.println("Make your choice");
            System.out.println("1.Add category, \n2.Add product, \n3.Remove Category, \n4.List of categories");
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    System.out.println("Add category");
                    System.out.println("Set name for category");
                    choose = scanner.next();
                    service.addCategory(choose);
                    break;
                case 2:
                    System.out.println("Give name of product,serial nubmer and category");
                    System.out.println("nameOfProduct,serialNumber,nameOfCategory");
                    String nameOfProduct = scanner.next();
                    int serialNumber = scanner.nextInt();
                    String nameOfCategory = scanner.next();
                    service.addProduct(nameOfProduct, serialNumber, new Category(nameOfCategory));
                    break;
                case 3:
                    System.out.println("Remove category");
                    choose = scanner.next();
                    service.removeCategory(choose);
                    break;
                case 4:
                    System.out.println("List of categories");
                    service.findAllCategories();
                    break;
                case 5:
                    System.out.println("exit");
                    break;
            }
        } while (input  != 5);

    }
}
