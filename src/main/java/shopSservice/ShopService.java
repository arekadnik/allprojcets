package shopSservice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ShopService {
    ;
    private ShopDao dao;

    public ShopService(ShopDao dao) {
        this.dao = dao;
    }

    public void addCategory(String name) {
        dao.addCategory(new Category(name));
    }

    public Collection<Category> findAllCategories() {


        return dao.allCategories();

    }

    public List<Product> findAllProducts() {

        List<Product> productList = new ArrayList<>();
        for (Category c : dao.allCategories()) {
            productList.addAll(dao.getProduct(c));
        }
        return productList;

    }

    public void removeProductByname(String name) {

        for (Category c : dao.allCategories()) {
            dao.getProduct(c).removeIf(p -> p.getName().equals(name));
        }
    }

    public void removeProductBySerial(int serial) {
        for (Category c : dao.allCategories()) {
            dao.getProduct(c).removeIf(p -> p.getSerial() == (serial));
        }
    }

    public void addProduct(String name, int serialNumber, Category category) {
        dao.addProduct(category, new Product(serialNumber, name));

    }

    public void removeCategory(String name) {
        if (!isCategoryempty(name)) {
            throw new RuntimeException("Category is not empty");
        }
        dao.removeCategory(new Category(name));
    }

    private boolean isCategoryempty(String name) {
        return false;
    }

    public void sortCategory() {
        Set<Category> categories = dao.allCategories();
        categories.stream().sorted().collect(Collectors.toList());
    }

    private boolean isAvailableProduct(String name) {
//        for (Category c : dao.allCategories())
//            if (dao.getProduct(c).equals(name)) {
//                return true;
//            }
//        return false;
        return false;
    }

    private Product findByName(String name) {
        return find(x -> x.getName().equals(name));
    }

    private Product findBySerial(int serial) {
        return find(x -> x.getSerial() == serial);
    }

    private Product find(Predicate<Product> predicate) {
        for (Product prduct : findAllProducts()) {
            if (predicate.test(prduct)) ;
            return prduct;
        }
        return null;
    }

    private Product findStream(Predicate<Product> predicate) {
        return findAllProducts().stream().filter(predicate).findFirst().get();
    }


}
