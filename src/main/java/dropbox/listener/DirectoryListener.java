package dropbox.listener;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.ArkadiuszStadnik.dropbox.config.Keys.DIRECTORY;

public class DirectoryListener {
    private Uploader uploader;
    private String dir;
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public DirectoryListener(Uploader uploader, ConfigService cfg) {
        this.uploader = uploader;
        this.dir = cfg.get(DIRECTORY);
    }

    public void listen() throws IOException, InterruptedException {

        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path path = Paths.get(dir);
        path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        WatchKey key;
        while ((key = watchService.take()) != null) {
            //TODO check if the is only single event
            String name = key.pollEvents().get(0).context().toString();
            System.out.println(name);
            executorService.submit(() -> uploader.upload(dir + name, name));
            key.reset();
        }
    }
}
