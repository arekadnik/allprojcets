package dropbox.mail.client;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.Email;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Emailv31;
import org.json.JSONArray;
import org.json.JSONObject;

import static com.ArkadiuszStadnik.dropbox.config.Keys.*;

public class MailJetProvider implements EmailClient {

    public MailJetProvider(ConfigService configService) {
        this.configService = configService;
    }

    private ConfigService configService;
    //TODO czemu tu jest null ?


    //TODO nie wiem czy to dobrze jest , czy ta klasa powinna implementowac interfejs?
    @Override
    public void send(Email email) {
        final String apiKey = configService.get(API_KEY_MailJEt);
        final String secretApiKey = configService.get(SECRET_API_KEY_MailJet);
        MailjetClient client;
        MailjetRequest request;
        MailjetResponse response;
        client = new MailjetClient(apiKey, secretApiKey, new ClientOptions("v3.1"));
        request = new MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, new JSONArray()
                        .put(new JSONObject()
                                .put(Emailv31.Message.FROM, new JSONObject()
                                        .put("Email", configService.get(EMAIL_FROM))
                                        .put("Name", "Mailjet Pilot"))
                                .put(Emailv31.Message.TO, new JSONArray()
                                        .put(new JSONObject()
                                                .put("Email", configService.get(EMAIL_ADDRESS))
                                                .put("Name", "passenger 1")))
                                .put(Emailv31.Message.SUBJECT, configService.get(EMAIL_SUBJECT))
                                .put(Emailv31.Message.TEXTPART, configService.get(EMAIL_CONTENT))
                                .put(Emailv31.Message.HTMLPART, configService.get(EMAIL_CONTENT))));
        try {
            client.post(request);
        } catch (MailjetException | MailjetSocketTimeoutException e) {
            throw new SendingEmailException("Could not send email to:" + email.getTo(), e);
        }

    }
}
