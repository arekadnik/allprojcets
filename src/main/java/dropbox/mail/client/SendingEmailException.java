package dropbox.mail.client;

public class SendingEmailException extends RuntimeException
{

    public SendingEmailException(String message,Throwable cause){
        super(message,cause);
    }

}

