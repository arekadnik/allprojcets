package dropbox.mail.client;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.Email;
import com.sendgrid.*;

import java.io.IOException;

import static com.ArkadiuszStadnik.dropbox.config.Keys.*;


public class SendGridProvider implements EmailClient {
//    private final String SENDGRID_API_KEY = "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";
//    ConfigService configService;

    private final ConfigService configService;

    public SendGridProvider(ConfigService configService) {
        this.configService = configService;
    }

    @Override
    public void send(Email email) {

        com.sendgrid.Email from = new com.sendgrid.Email(configService.get(EMAIL_FROM));
        String subject = configService.get(EMAIL_SUBJECT);
        com.sendgrid.Email to = new com.sendgrid.Email(configService.get(EMAIL_ADDRESS));
        Content content = new Content("text/plain", configService.get(EMAIL_CONTENT));
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid(configService.get(SEND_GRID_KEY));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sg.api(request);
        } catch (IOException ex) {
            throw new SendingEmailException("Could not send email to:" + email.getTo(), ex);
        }
    }
}



