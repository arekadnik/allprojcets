package dropbox.mail;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.client.EmailClient;

import static com.ArkadiuszStadnik.dropbox.config.Keys.*;

public class EmailService {


    private ConfigService configService;
    //TODo wydaje mi sie ze powinno przyjmowac mailrpovider
    private EmailClient emailClient;

    public EmailService(ConfigService configService, EmailClient emailClient) {
        this.configService = configService;
        this.emailClient = emailClient;
    }
    public void onNewFile(String fileName) {
        Email e = new Email(configService.get(EMAIL_ADDRESS), configService.get(EMAIL_CONTENT), configService.get(EMAIL_SUBJECT));
        emailClient.send(e);
    }
}
