package dropbox.upload;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.mail.EmailService;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.users.FullAccount;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.ArkadiuszStadnik.dropbox.config.Keys.DROPBOX_TOKEN;

public class DropBoxUploader implements Uploader {

    private final ConfigService cfg;
    private final EmailService emailService;
//    private final Email email;


    public DropBoxUploader(ConfigService cfg, EmailService emailService) {
        this.cfg = cfg;
        this.emailService = emailService;
//        this.email = email;
    }

    @Override
    public void upload(String path, String name) {
        String token = cfg.get(DROPBOX_TOKEN);
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, token);
        FullAccount account = null;
        try {
            account = client.users().getCurrentAccount();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        System.out.println(account.getName().getDisplayName());
        try (InputStream in = new FileInputStream(path)) {
            FileMetadata metadata = client.files().uploadBuilder("/" + name).uploadAndFinish(in);
            emailService.onNewFile(name);
        } catch (IOException | DbxException e) {
            throw new UploadExcetpion("can not upload file" + path, e);
        }
    }
}
