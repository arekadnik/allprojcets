package dropbox.config;

public interface Keys {

    String DROPBOX_TOKEN = "DROPBOX_TOKEN";
    String DIRECTORY = "DIRECTORY";
    String MAIL_CLIENT = "MAIL_CLIENT";
    String EMAIL_FROM = "EMAIL_FROM";
    String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    String EMAIL_SUBJECT = "EMAIL_SUBJECT";
    String EMAIL_CONTENT = "EMAIL_CONTENT";
    String API_KEY_MailJEt = "API_KEY_MailJEt";
    String SECRET_API_KEY_MailJet = "SECRET_API_KEY_MailJet";
    String SEND_GRID_KEY = "SEND_GRID_KEY";


}
