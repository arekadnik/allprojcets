package dropbox;

import com.ArkadiuszStadnik.dropbox.config.ConfigService;
import com.ArkadiuszStadnik.dropbox.listener.DirectoryListener;
import com.ArkadiuszStadnik.dropbox.mail.EmailService;
import com.ArkadiuszStadnik.dropbox.mail.MailProvider;
import com.ArkadiuszStadnik.dropbox.mail.client.EmailClient;
import com.ArkadiuszStadnik.dropbox.upload.DropBoxUploader;

import java.io.IOException;

public class Main {
    private static final int PROPS_INDEX = 0;

    public static void main(String[] args) throws IOException, InterruptedException {
        String propsPath = args[PROPS_INDEX];
        ConfigService cfg = new ConfigService(propsPath).load();
//        EmailClient emailClient = MailProvider.get(cfg);
        MailProvider mailProvider = new MailProvider();
        EmailClient emailClient = mailProvider.get(cfg);
        EmailService emailService = new EmailService(cfg,emailClient);
        emailService.onNewFile("yoy");
        DropBoxUploader dropBoxUploader = new DropBoxUploader(cfg,emailService);
        DirectoryListener directoryListener = new DirectoryListener(dropBoxUploader,cfg);
        directoryListener.listen();







//        MailProvider mailProvider = new MailProvider();
//        EmailService emailService = new EmailService();
//        DropBoxUploader dropBoxUploader = new DropBoxUploader(cfg, emailClient);
//        DirectoryListener directoryListener = new DirectoryListener(dropBoxUploader,cfg);
//        directoryListener.listen();

    }
}
